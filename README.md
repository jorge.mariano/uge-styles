# UGE Styles

## Description

Ce projet regroupe un ensemble d'outils et contributions pour faciliter l'utilisation des outils LaTex (&cie) dans le contexte de l'Université Gustave Eiffel (UGE)

## Diffusion

Les contributions sont disponibles via la plateforme Overleaf

* (UGE-Beamer) https://www.overleaf.com/read/cvsxctmgkgrc 
* (UGE-Template) https://www.overleaf.com/read/skbnmsrgffxy
* (UGE-Poster) https://www.overleaf.com/read/ymnkztszfqdd 
* (JMR.sty) https://www.overleaf.com/read/bvzwvqywwskb

Accessibles en lecture seule. Il est nécessaire de dupliquer ces projets pour les utiliser (et potentiellement les modifier).

En cas de modification des fichiers «UGE», merci de bien vouloir faire un retour afin d'éventuellement améliorer la fourniture.

## UGE Beamer

Ce template est une transcription LaTeX du modèle officiel Powerpoint fourni par la charte UGE.

## UGE Template

Les élements généraux de mise en conformité avec la charte graphique UGE

* La police de caractère
* Les (11) couleurs standardisées
* Les conventions typographiques principales
* Entêtes et pieds de pages (UGEfancy pagestyle) 

## UGE Template Thesis

* La première page d'une thèse
* Les éléments constitutifs du jury
* La bibliographie (biblatex & zotero recommandés !)

## UGE Poster

Une première ébauche expérimentale par reprise d'un template pré-existant.

## JMR.sty

Ensemble d'extensions (package) utilisés pour faciliter le processus de rédaction LaTeX.
Recueil des bonnes pratiques et astuces (format de page, typographie, debug, biblatex, etc)

## Roadmap

* Template UGE-Beamer &  Template UGE / Couleurs
* Intégrer les métadonnées, titre, auteur, (...) dans le PDF résultant (e.g. intégration/test dans Calibre)
* Proposer une génération ePUB (https://tex.stackexchange.com/questions/649009/latex-to-epub-conversion ?)

## Contribution & retours

* Première expérimentation pour le template de thèse : Dylan Moinse, Alain Lhostis

## Authors and acknowledgment

* Jorge Mariano (jorge.mariano@univ-eiffel.fr)
* ... autres à compléter

## License / Licence

À définir

## Status

Expérimentations en cours, plus ou moins utilisables
